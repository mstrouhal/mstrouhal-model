<?php

/**
 * @package:
 * @subpackage:
 * @author: Martin Strouhal <martin@martinstrouhal.cz>, <martin@smartemailing.cz>
 * Created on: 28.04.13 0:14
 */

namespace MStrouhal\Model;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Object;

class EntityCollection extends Object implements \Iterator, \Countable {

	/**
	 * @var Selection
	 */
	private $selection;

	/**
	 * @var Entity[]
	 */
	protected $data = null;

	/**
	 * @var array
	 */
	protected $keys;

	/**
	 * @var string
	 */
	protected $entityClass;

	/**
	 * @var string
	 */
	protected $refTable;

	public function __construct(Selection $selection, $entityClass, $refTable = null) {
		$this->selection = $selection;
		$this->entityClass = $entityClass;
		$this->refTable = $refTable;
	}

	protected function loadData() {
		/** @var $row ActiveRow */
		if ($this->data === null) {
			$this->data = array();
			if ($this->refTable) {
				foreach ($this->selection as $row) {
					$this->data[] = new $this->entityClass($row->ref($this->refTable));
				}
			}
			else {
				foreach ($this->selection as $row) {
					$this->data[] = new $this->entityClass($row);
				}
			}
		}
	}

	/** @return void */
	function rewind() {
		$this->loadData();
		$this->keys = array_keys($this->data);
		reset($this->keys);
	}


	/** @return Entity */
	function current() {
		$key = current($this->keys);
		return $key === FALSE ? FALSE : $this->data[$key];
	}


	/** @return mixed */
	function key() {
		return current($this->keys);
	}


	/** @return void */
	function next() {
		next($this->keys);
	}


	/** @return bool */
	function valid() {
		return current($this->keys) !== FALSE;
	}

	/** @return int */
	function count() {
		$clone = clone $this->selection;
		try {
			$column = $clone->getPrimary();
		}
		catch (\LogicException $e) {
			$column = '*';
		}
		return $clone->count($column);
	}


}