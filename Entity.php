<?php

/**
 * @package:
 * @subpackage:
 * @author: Martin Strouhal <martin@martinstrouhal.cz>, <martin@smartemailing.cz>
 * Created on: 27.04.13 22:22
 */


namespace MStrouhal\Model;

use Nette\Database\Table\ActiveRow;
use Nette\Object;

/**
 * Class Entity
 * @property-read int $id
 */
abstract class Entity extends Object {

	/**
	 * @var ActiveRow
	 */
	protected $activeRow;

	/**
	 * @var array
	 */
	protected $updates = array();

	/**
	 * @var bool
	 */
	protected $isInDatabase = false;

	public function __construct(ActiveRow $row = null) {
		if ($row) {
			$this->activeRow = $row;
			$this->isInDatabase = true;
		}
	}

	public function isInDatabase() {
		return $this->isInDatabase;
	}

	public function &__get($name) {
		if (array_key_exists($name, $this->updates)) {
			return $this->updates[$name];
		}
		if (!$this->isInDatabase) {
			$null = null;
			return $null;
		}
		return $this->activeRow->__get($name);
	}

	public function __set($name, $value) {
		$methodName = 'set' . lcfirst(strtolower($name));
		if (static::getReflection()->hasMethod($methodName)) {
			$this->$methodName($value);
		}
		else {
			$this->updates[$name] = $value;
		}
	}

	public function getActiveRow() {
		return $this->activeRow;
	}

	public function getId() {
		return $this->activeRow->id;
	}

	public final function setId($value) {
		throw new \LogicException('ID is read-only property');
	}

	protected function getMany($entityName, $pairTable = null, $refTable = null) {

		if (!$refTable) {
			$parts = explode('\\', $entityName);
			$refTable = strtolower(end($parts));
		}

		if (!$pairTable) {
			$pairTable = strtolower($this->reflection->shortName) . '_' . $refTable;
		}

		$sel = $this->activeRow->related($pairTable);
		return new EntityCollection($sel, $entityName, $refTable);
	}

	public function toArray() {
		if ($this->isInDatabase) {
			return array_replace($this->activeRow->toArray(), $this->updates);
		}
		return $this->updates;
	}

	public function needToPersist() {
		if (!$this->isInDatabase || !empty($this->updates)) {
			throw new \LogicException('Entity must be persisted first');
		}
	}


}