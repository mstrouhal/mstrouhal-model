<?php

/**
 * @package:  MStrouhal\Model
 * @subpackage: orm
 * @author: Martin Strouhal <martin@martinstrouhal.cz>, <martin@smartemailing.cz>
 * Created on: 27.04.13 21:42
 */


namespace MStrouhal\Model;

use Nette\Database\Connection;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\Object;

abstract class Repository extends Object {

	/**
	 * @var Connection
	 */
	protected $connection;

	/**
	 * @var string
	 */
	protected $tableName;

	/**
	 * @var string
	 */
	protected $entityClass;

	/**
	 * @var bool
	 */
	protected $inTransaction = false;

	public function __construct(Connection $connection) {

		$this->connection = $connection;

		if ($this->reflection->hasAnnotation('table')) {
			$this->tableName = $this->reflection->getAnnotation('table');
		}

		else {
			preg_match('#(\w+)Repository$#', get_class($this), $m);
			if (!isset($m[1])) {
				throw new \LogicException('Invalid repository name. Must be (\w+)Repository');
			}
			$this->tableName = lcfirst($m[1]);
		}

		if ($this->reflection->hasAnnotation('entityClass')) {
			$this->entityClass = $this->reflection->getAnnotation('entityClass');
		}
		else {
			$this->entityClass = ucfirst($this->tableName);
		}
	}

	protected function createEntity(ActiveRow $row) {
		if (!$row) {
			return false;
		}
		return new $this->entityClass($row);
	}

	public function getTable() {
		return $this->connection->table($this->tableName);
	}

	/**
	 * @param $pkVal
	 * @return bool|Entity
	 */
	public function find($pkVal) {
		/** @noinspection PhpParamsInspection */
		return $this->createEntity($this->getTable()->select('*')->where($this->getTable()->getPrimary(), $pkVal)->fetch());
	}

	public function persist(Entity &$entity) {

		$prop = $entity->getReflection()->getProperty('updates');
		$prop->setAccessible(true);
		$data = $prop->getValue($entity);

		if (empty($data)) {
			return;
		}

		if ($entity->isInDatabase()) {
			$this->connection
				->table($this->tableName)
				->wherePrimary($entity->getActiveRow()->getPrimary())
				->update($data);
		}
		else {
			$activeRow = $this->connection
				->table($this->tableName)
				->insert($data);
			$entity = new $this->entityClass($activeRow);
		}

	}

	public function copy(Entity $entity) {
		$values = $entity->getActiveRow()->toArray();
		unset($values['id']);
		$row = $this->connection->table($this->tableName)->insert($values);
		$clone = new $this->entityClass($row);

		$prop = $entity->getReflection()->getProperty('updates');
		$prop->setAccessible(true);
		$updates = $prop->getValue($entity);

		foreach ($updates as $key => $val) {
			$clone->$key = $val;
		}

		return $clone;

	}

	public function findAll() {
		return new EntityCollection($this->getTable(), $this->entityClass);
	}

	public function findOneBy($where) {
		$args = func_get_args();
		/** @var $selection Selection */
		$selection = call_user_func_array(array($this->getTable(), 'where'), $args)->order('id ASC')->limit(1);
		if ($selection === false) {
			throw new \RuntimeException('An unexpected error occured when assembling selection');
		}
		$row = $selection->fetch();
		return $row ? $this->createEntity($row) : false;
	}

	public function findBy($where) {
		$args = func_get_args();
		$selection = call_user_func_array(array($this->getTable(), 'where'), $args)->order('id ASC');
		if ($selection === false) {
			throw new \RuntimeException('An unexpected error occured when assembling selection');
		}
		return new EntityCollection($selection, $this->entityClass);
	}

	public function remove(Entity &$entity) {
		if (!$entity->isInDatabase()) {
			throw new \LogicException('Deleted entity does not exist in the database');
		}

		$data = $entity->getActiveRow()->toArray();

		$prop = $entity->getReflection()->getProperty('updates');
		$prop->setAccessible(true);
		$unsaved = $prop->getValue($entity);

		$defaults = array_merge($data, $unsaved);

		unset($defaults['id']);

		$entity->getActiveRow()->delete();

		/** @var $entity Entity */
		$entity = new $this->entityClass();

		$prop = $entity->getReflection()->getProperty('updates');
		$prop->setAccessible(true);
		$prop->setValue($entity, $defaults);

	}

	public function begin() {
		if (!$this->inTransaction) {
			$this->connection->beginTransaction();
			$this->inTransaction = true;
		}
	}

	public function rollback() {
		if ($this->inTransaction) {
			$this->connection->rollBack();
			$this->inTransaction = false;
		}
	}

	public function commit() {
		if ($this->inTransaction) {
			$this->connection->commit();
			$this->inTransaction = false;
		}
	}


}
